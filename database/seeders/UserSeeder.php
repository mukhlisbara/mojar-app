<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id_role'              => '1',
            'name'              => 'Mukhlis Bara Pamungkas',
            'email'             => 'bara@bara.com',
            'password'          => bcrypt('qweqweqwe'),

        ]);
    }
}

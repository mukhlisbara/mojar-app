<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link c-active" href="/home-kaprodi">
        <i class="c-sidebar-nav-icon cil-home"></i>Home
    </a>
    {{-- <a class="c-sidebar-nav-link c-active" href="/super-admin-kelola-akun">
        <i class="c-sidebar-nav-icon cil-people"></i>Super Admin
    </a> --}}
    <a class="c-sidebar-nav-link c-active" href="/daftar-tim-pengusul">
        <i class="c-sidebar-nav-icon cil-history"></i>Tim Pengusul
    </a>
    <a class="c-sidebar-nav-link c-active" href="/daftar-tim-reviewer">
        <i class="c-sidebar-nav-icon cil-history"></i>Tim Reviewer
    </a>
    <a class="c-sidebar-nav-link c-active" href="/dokumen">
        <i class="c-sidebar-nav-icon cil-file"></i>Dokumen Pengadaan
    </a>
    <a class="c-sidebar-nav-link c-active" href="/timeline-kaprodi">
        <i class="c-sidebar-nav-icon cil-calendar"></i>Timeline
    </a>
    {{-- <a class="c-sidebar-nav-link c-active" href="/detail-matkul">
        <i class="c-sidebar-nav-icon cil-book"></i>Detail Matkul
    </a> --}}
</li>
